## Editor Tool
Download VisualStudio Code at [here](https://code.visualstudio.com/Download).
## How to install the extension 
### PHP IntelliSense 1.5.4
1. Open VisualStudio Code
2. Clicking on the `Extensions` icon in the `Activity Bar` on the side of `VS Code` or the `View: Extensions` command (`⇧⌘X`)
3. Input `PHP IntelliSense` into search field then `Enter`
4. Select `PHP IntelliSense` then click **Install**
5. After install successful. Click **Reload**
### PSR-2: Coding Style
[Reference document](https://github.com/Dickurt/vscode-php-formatter)
1. Download `php-cs-fixer`. Run commands:
```
	curl -L http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -o php-cs-fixer 	&&
	sudo chmod a+x php-cs-fixer														&&
	sudo mv php-cs-fixer /usr/local/bin/php-cs-fixer								&&
	php-cs-fixer
```
[Reference Document](https://github.com/FriendsOfPHP/PHP-CS-Fixer#installation)
2. Add extension `PHP Formatter`
	a) Clicking on the `Extensions` icon in the `Activity Bar` on the side of `VS Code` or the `View: Extensions` command (`⇧⌘X`)
	b) Input `PHP Formatter` into search field then `Enter`
	c) Select `PHP Formatter` then click **Install**
    d) After install successful. Click **Reload**
3. VSCode user settings
`File > Preferences > Settings (Code > Preferences > Settings on Mac)`. Editing `settings.json`
	**With Composer**
	1. Add Composer to your PATH environment variable.
	2. Add `"phpformatter.composer": true` to your VSCode user settings.
	![Setting php-cs-fixer](https://github.com/Dickurt/vscode-php-formatter/raw/master/images/install-composer.jpg?raw=true)
	**Manual**
	1. Add PHP to your PATH environment variable. I.e. ensure that `php -v` works. Otherwise use `"phpformatter.phpPath" = "/path/to/php/executable"`.
	2. Point the `phpformatter.pharPath` setting to where you put the php-cs-fixer file.
	![Setting php-cs-fixer](https://github.com/Dickurt/vscode-php-formatter/raw/master/images/install-manual.jpg?raw=true)
	
4. Reload VSCode
